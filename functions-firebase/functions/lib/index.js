"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const moment = require("moment");
const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");
'use strict';
//The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const types = [{
        id: '0',
        name: 'developer'
    }, {
        id: '1',
        name: 'designer'
    }, {
        id: '2',
        name: 'business'
    }];
const validateType = (type) => {
    //return (type === dev) || (type === designer) || (type === business)
    return !!types.find((obj) => { return obj.id === type.toString(); });
};
const validateEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
};
const saveEmail = (request, response) => {
    if (request.method === 'POST') {
        let result = {};
        const mType = request.body.type;
        const mEmail = request.body.email;
        console.log("request.body " + request.body);
        if (mType == undefined) {
            result = { "status": "error", "message": "No type defined!" };
            response.send(result);
        }
        else if (mEmail == undefined) {
            result = { "status": "error", "message": "No email defined!" };
            response.send(result);
        }
        else if (!validateType(mType) || !validateEmail(mEmail)) {
            result = { "status": "error", "message": "data format invalid" };
            response.send(result);
        }
        else {
            save(mType, mEmail);
        }
        function save(type, email) {
            const date = moment().format("MMM Do YY");
            const time = moment().format("h:mm:ss a");
            const keyEmail = email.substring(0, email.lastIndexOf("."));
            admin.database().ref(`/email/${types[type].name}/${keyEmail}`).set({ email, date, time }).then(snapshot => {
                result = { "status": "success", "message": "email saved" };
                response.send(result);
            });
        }
    }
};
const app = express();
app.use(cors({ origin: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.post('/', (req, res) => {
    saveEmail(req, res);
});
exports.sendemail = functions.https.onRequest(app);
// Start writing Firebase Functions
// https://firebase.google.com/functions/write-firebase-functions
// exports.sendemail = functions.https.onRequest((request, response) => {
//   if(request.method === 'POST'){
//     let result: any = {}
//     const mType = request.body.type
//     const mEmail = request.body.email
//     console.log("request.body " + request.body)
//     console.log("request.body.email " + request.body.email)
//     console.log("mEmail " + mEmail)
//     // console.log(mEmail)
//     if(mType == undefined){
//       result = { "status" : "error", "message" : "No type defined!"}
//       response.status(400).send(result)
//     }else if(mEmail == undefined){
//       result = { "status" : "error", "message" : "No email defined!"}
//       response.status(400).send(result)
//     }else{
//       checkData(parseInt(mType), mEmail)
//     }
//     function checkData(type:number, email:String){
//       if(validateType(type)){
//           if(validateEmail(email)){
//             saveEmail(type, email)
//           }else {
//             result = { "status" : "error", "message" : "email format invalid"}
//             response.send(result)
//             console.log(result)
//             console.log(response)
//           }
//       }else {
//           result = { "status" : "error", "message" : "type format invalid"}
//           response.send(result)
//           console.log(result)
//           console.log(response)
//       }
//     }
//     function saveEmail(type:number, email:String){
//       const date = moment().format("MMM Do YY")
//       const time = moment().format("h:mm:ss a")
//       // admin.database().ref(`/email/${types[type].name}`).push({ email, date, time }).then(snapshot => {
//       //   result = { "status" : "success", "message" : "email saved"}
//       //   response.status(200).send(result)
//       //   console.log(result)
//       //   console.log(response)
//       // });
//     }
//   }
// })   
// app.post('/', (req, res) => sendmail(req, res))
// exports.sendemail = functions.https.onRequest(app)
// import * as functions from 'firebase-functions'
// //The Firebase Admin SDK to access the Firebase Realtime Database.
// const admin = require('firebase-admin')
// admin.initializeApp(functions.config().firebase);
// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
// const types = [{
//   id: '0',
//   name: 'developer'
// }, {
//   id: '1',
//   name: 'designer'
// }, {
//   id: '2',
//   name: 'business'
// }]
// const validateType = (type : number) => {
//   //return (type === dev) || (type === designer) || (type === business)
//   return !!types.find((obj) => { return obj.id === type.toString()})
// }
// const validateEmail = (email : String) => {
//   const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//   return re.test(email.toLowerCase())
// }
// exports.sendemail = functions.https.onRequest((request, response) => {
//   if(request.method === 'GET'){
//     const mType = request.param('type')
//     const mEmail = request.param('email')
//     let result: any = {}
//     console.log(mType)
//     console.log(mEmail)
//     checkData(parseInt(mType), mEmail)
//     function checkData(type:number, email:String){
//       if(validateType(type)){
//           if(validateEmail(email)){
//             saveEmail(type, email)
//           }else {
//             result = { "status" : "error", "message" : "email format invalid"}
//             response.send(result)
//             console.log(result)
//             console.log(response)
//           }
//       }else {
//           result = { "status" : "error", "message" : "type format invalid"}
//           response.send(result)
//           console.log(result)
//           console.log(response)
//       }
//     }
//     function saveEmail(type:number, email:String){
//       admin.database().ref(`/email/${types[type].name}`).push({ email }).then(snapshot => {
//         result = { "status" : "success", "message" : "email saved"}
//         response.send(result)
//         console.log(result)
//         console.log(response)
//       });
//     }
//   }
// }) 
//# sourceMappingURL=index.js.map